(ns brukru.util.crypt-test
  (:use clojure.test
        brukru.util.crypt))

(deftest gen-stalt-test
  (testing "Salt of the default length"
    (is (= (Class/forName "[B") (.getClass (gen-salt))))))

(deftest random-string-test
  (testing "Test teh string length"
    (is (= 10 (.length (random-string 10))))))

(deftest base64-test
  (testing "base64-to-byte equals"
    (let [data (into-array Byte/TYPE "test")]
      (is (= data (base64-to-byte "test")))))
  (testing "base64-to-byte is byte array"
    (is (= (Class/forName "[B") (.getClass (base64-to-byte "test")))))
  (testing "byte-to-base64"
    (let [data (base64-to-byte "test")]
      (is (= "test" (byte-to-base64 data))))))