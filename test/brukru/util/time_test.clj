(ns brukru.util.time-test
  (:use clojure.test
        brukru.util.time))

(deftest sql-now-test
  (testing "Is a sql time"
    (is (instance? java.sql.Timestamp (sql-now)))))

(deftest datestr-test
  	(testing "Invalid format"
     (is (thrown-with-msg? Exception #"Invalid date format" (datestr "bad" "1")))))