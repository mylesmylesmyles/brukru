(ns brukru.server
  (:require [org.httpkit.server :refer [run-server]]
            [brukru.app :as app]
            [clojure.tools.logging :refer [info error]])
  (:gen-class))

(defn- add-port [options port]
  (let [port (if port
               (Integer/parseInt port)
               8080)]
    (assoc options :port port)))

(defn- parse-args [[port :as args]]
  (-> {}
      (add-port port)))

(defn -main [& args]
  (info "Starting server...")
  (let [options (parse-args args)
        port (:port options)
        server (run-server (var app/site-handler)
                                {:port port :join? false})]
    (info "Server started on port" port)
    (info (str "You can view the site at http://localhost:" port))
    server))

(defn stop [instance]
  (.stop instance)
  (info "Server stopped"))

