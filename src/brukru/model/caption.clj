(ns brukru.model.caption
	(:require [brukru.config :as config]
				[brukru.model.util :refer [update? clob-to-string]]
				[brukru.model.users :refer [users]]
				[korma.core :refer :all]
				[korma.db :refer [transaction]]
            	[clojure.tools.logging :refer [debug info error]]
	            [brukru.config :refer [PRIVATE-DIR]]
	            [brukru.view.common :refer [wrap-context]]))

; So from my understanding of this thread https://groups.google.com/forum/?fromgroups=#!topic/sqlkorma/SoMAteoUpug
; and this thread https://groups.google.com/forum/?fromgroups=#!topic/h2-database/41nMmjcX1ZM
; Since I want to convert my clobs to strings in the resultset so that my code doesn't know the
; difference, I have to wrap my select statements in transactions since they are prepared statements
; which are re-executed



; A picture with captions
(defentity picture
	(table :caption_picture)
	(transform #(-> %
		(update? :location clob-to-string))))

; A caption entry for a picture
(defentity entry
	(table :caption_entry)
	(belongs-to picture {:fk :picture})
	(belongs-to users {:fk :author})
	(transform #(-> %
		(update? :entry clob-to-string))))

; Base entry fields for select
(def ^{:private true} base-entry (-> (select* entry)
					(with users)
					(fields [:users.id :user-id]
							[:users.name :user-name]
							[:users.nickname :user-nickname]
							:caption_entry.id
							:caption_entry.entry
							:caption_entry.createdon
							:caption_entry.rank
							:caption_entry.picture)
					(order :caption_entry.rank :DESC)
					(order :caption_entry.createdon :DESC)))

; A vote for a caption entry
(defentity entry-vote
	(table :caption_entry_vote)
	(belongs-to entry {:fk :entry})
	(belongs-to users {:fk :author}))


(def ^{:private true} user-stats (-> (select* users)
					(fields :id
							:name
							:email
							:nickname
							:lastlogin
							:disabled)
					(join entry (= :caption_entry.author :users.id))
					(aggregate (count :caption_entry.id) :total-entries)
					(aggregate (avg :caption_entry.rank) :avg-rank)
					(group :users.id)
					(group :users.name)
					(group :users.nickname)
					(order :id :ASC)))

(defn get-user-stats
	([id] (select user-stats (where {:id id})))
	([] (select user-stats)))

(defn get-current-picture 
	"Return the current picture which is to be used for adding captions"
	[]
	(if-let [picture (transaction (first (select picture (where {:canaddcaptions true}) (order :id :DESC))))]
		picture
		{})
	)

(defn get-picture 
	"Return a picture by id"
	[^Integer id]
	(if-let [pic (transaction (first (select picture (where {:id id}))))]
		pic
		{}))

(defn get-entries 
	"Return the list of entries belonging to a picture"
	([pic lim off]
		(transaction 
			(select base-entry (where {:picture (pic :id)})
					(limit lim)
					(offset off))))
	([pic lim]
		(transaction 
			(select base-entry (where {:picture (pic :id)})
					(limit lim))))
	([pic]
		(transaction 
			(select base-entry (where {:picture (pic :id)})))))

(defn get-entries-for-user 
	"Return the list of entries belonging to a particular author"
	([usr lim off]
		(transaction
			(select base-entry (where {:author (usr :id)})
					(limit lim)
					(offset off))))
	([usr lim]
		(transaction
			(select base-entry (where {:author (usr :id)})
					(limit lim))))
	([usr]
		(transaction
			(select base-entry (where {:author (usr :id)})))))

(defn get-entry
	"Return an entry by id"
	[id]
	(transaction (first (select base-entry (where {:id id})))))

(defn entry-exists?
	"Does an entry exist?"
	[e]
	(not (empty? (get-entry (e :id)))))

(defn save-entry 
	"Save a new entry"
	[new-entry]
	(transaction 
		(if (or (nil? (new-entry :id)) (not (entry-exists? (new-entry :id))))
			(insert entry (values new-entry))
			(update entry (set-fields new-entry)
				(where {:id (new-entry :id)})))))

(defn has-voted? 
	"Has the user already voted for an entry?"
	[entry user]
	(-> (transaction (select entry-vote (where {:author (user :id) :entry (entry :id)}))) empty? not))

(defn vote-for-entry 
	"Add a vote to an entry by a user"
	[for-entry user]
	(transaction 
		(if (and (entry-exists? for-entry) (not (has-voted? for-entry user)))
			(do
				(insert entry-vote (values {:author (user :id) :entry (for-entry :id)}))
				(update entry (set-fields {:rank (+ (for-entry :rank) 1)})
					(where {:id (for-entry :id)}))))))

