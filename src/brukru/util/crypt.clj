(ns brukru.util.crypt
  "Simple functions for hashing strings and comparing them. Typically used for storing passwords.  
Derived from http://www.javacodegeeks.com/2012/05/secure-password-storage-donts-dos-and.html"
	(:refer-clojure :exclude [compare])
	(:import (java.security SecureRandom NoSuchAlgorithmException))
	(:import (java.security.spec KeySpec InvalidKeySpecException))
	(:import (java.util Arrays))
	(:import (sun.misc BASE64Decoder BASE64Encoder))
	(:import (javax.crypto SecretKeyFactory))
	(:import (javax.crypto.spec PBEKeySpec)))

; PBKDF2 with SHA-1 as the hashing algorithm. Note that the NIST
; specifically names SHA-1 as an acceptable hashing algorithm for PBKDF2
(def algorithm "PBKDF2WithHmacSHA1")

; SHA-1 generates 160 bit hashes, so that's what makes sense here
(def derivedKeyLength 160)

; Pick an iteration count that works for you. The NIST recommends at
; least 1,000 iterations:
; http://csrc.nist.gov/publications/nistpubs/800-132/nist-sp800-132.pdf
; iOS 4.x reportedly uses 10,000:
; http://blog.crackpassword.com/2010/09/smartphone-forensics-cracking-blackberry-backup-passwords/
(def iterations 20000)

(defn gen-salt
	"Generate a random salt for encrypting strings"
	([size]
			; VERY important to use SecureRandom instead of just Random
			(let [random (SecureRandom/getInstance "SHA1PRNG")
				salt (byte-array size)]
				(.nextBytes random salt)
				salt))
	([] (gen-salt 8)))

(defn encrypt
	"Encrypt the given string with a generated or supplied salt. 
Uses PBKDF2 for strong hashing."
	;; generate a salt
	([salt raw] 
		(let [spec (PBEKeySpec. (char-array raw) salt iterations derivedKeyLength)
			f (SecretKeyFactory/getInstance algorithm)]
			(.getEncoded (.generateSecret f spec))))
	([raw] (encrypt (gen-salt) raw)))

(defn compare
	"Compare a raw string with an already encrypted string.  
Authentication succeeds if encrypted string supplied is 
equal to the raw string ecrypted with the salt"
	[raw encrypted salt] (Arrays/equals encrypted (encrypt salt raw)))

(defn random-string [length]
	"Generate a random ascii string"
	(let [ascii-codes (concat (range 48 58) (range 66 91) (range 97 123))]
		(apply str (repeatedly length #(char (rand-nth ascii-codes))))))

(defn base64-to-byte
	"From a base 64 representation, returns the corresponding byte[] 
@param data String The base64 representation
@return byte[]
@throws IOException"
	[ data]
		(let [decoder (BASE64Decoder.)]
			(.decodeBuffer decoder data)))

(defn byte-to-base64
	"From a byte[] returns a base 64 representation
	@param data byte[]
	@return String
	@throws IOException"
	[data]
		(let [encoder (BASE64Encoder.)]
			(.encode encoder data)))
   