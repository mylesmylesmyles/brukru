(ns brukru.util.time
  (:require [brukru.config :as config]
            [clj-time.core :as time]
            [clj-time.coerce :as coerce]
            [clj-time.format :as format]))

(defn- tz [x]
  "Convert a timezone string into a timezone obj"
  (time/time-zone-for-id x))

; Timezones
(def ^{:private true} UTC time/utc)
(def ^{:private true} TIME-ZONE (tz config/TIME-ZONE))

; Different date formats
(def ^{:private true} DATE-FORMATS
     {:short  (format/formatters :date TIME-ZONE)
      :pretty (format/formatter config/TIME-FORMAT TIME-ZONE)
      :rfc822 (format/formatters :rfc822 TIME-ZONE)})

; Convert different types of objects to a date string in the
; in the appropriate format
(defmulti parse-string (fn [x formatter] (class x)))
(defmethod parse-string java.util.Date
  [d formatter] 
  (format/unparse formatter (coerce/from-date d)))
(defmethod parse-string java.sql.Timestamp 
  [d formatter]
  (format/unparse formatter (coerce/from-sql-date d)))
(defmethod parse-string :default 
  [d formatter]
  (format/unparse formatter d))

(defn sql-now []
  "Return a new sql timestamp"
  (java.sql.Timestamp. (.getTimeInMillis (java.util.Calendar/getInstance))))

(defn datestr [format d]
  "Return a date as a string in a specific format"
  (if-let [f (DATE-FORMATS format)]
    (parse-string d f)
    (throw (Exception. (str "Invalid date format " format)))))



