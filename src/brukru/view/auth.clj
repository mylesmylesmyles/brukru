(ns brukru.view.auth
  (:require [clojure.string :refer [blank? lower-case]]
            [clojure.tools.logging :refer [debug info error]]
            [compojure.core :refer [defroutes GET POST]]
            [ring.util.response :as response]
            [hiccup.core :as hiccup]
            [brukru.util.session :as session]
            [brukru.util.time :refer [sql-now]]
            [brukru.view.common :refer [wrap-context wrap-layout render-template error-alert]]
            [brukru.model.users :refer [get-user-by-email too-many-login-attempts? valid-credentials? record-login-failure save-user]]))

(defn- init-session
    "Initialise session with user data"
    [user]
    (session/set-user! user))

(defn- login-page-body [request]
    (render-template "login-form.html" {}))

(defn- forgot-page-body [request]
    (render-template "forgot-password.html" {}))

(defn- registration-page-body [request]
    (render-template "register.html" {}))

(defn- login-page [request]
  (wrap-layout "Log in"
    (hiccup/html [:div
            (if (not (nil? (-> request :flash :error-message)))
                (error-alert (-> request :flash :error-message)))
            (login-page-body request)])))

(defn- login-success [user]
    (info "Login success")
    -(init-session user)
    -(save-user (assoc user :lastlogin (sql-now)))
    (response/redirect (wrap-context "/")))

(defn- login-fail 
    ([request ^String message]
        (info "Login failed [with message]")
        (assoc (response/redirect (wrap-context "/login")) :flash {:error-message message :prev-params (request :params)}))
    ([request]
        (info "Login failed")
        (assoc (response/redirect (wrap-context "/login")) :flash {:prev-params (request :params)})))

(defn- login-action [request]
    (info "Logging user in")
    (let [login (-> request :params :login) password (-> request :params :password)]
        (if (or (empty? login) (clojure.string/blank? password))
            (login-fail request "Missing email or password"))
        (let [user (get-user-by-email (lower-case login))]
            (cond
                (empty? user) 
                    (login-fail request "Your email and password are incorrect")
                (too-many-login-attempts? (user :id)) 
                    (login-fail request "Your account has been temporarily locked for too many failed login attempts")
                (not (valid-credentials? (user :password) (user :salt) password)) 
                    (do 
                        (record-login-failure (user :id)) 
                        (login-fail request "Your email and password are incorrect"))
                :else 
                    (login-success user)))))

(defn- logout-page [request]
    (session/logout)
    (response/redirect (wrap-context "/")))

(defn- forgot-page [request]
    (wrap-layout "Forgot password"
        (forgot-page-body request)))

(defn- registration-page [request]
    (wrap-layout "Registration"
                (registration-page-body request)))

(defroutes auth-routes
    (GET "/login" request (login-page request))
    (POST "/login" request (login-action request))
    (GET "/logout" request (logout-page request))
    (GET "/forgot" request (forgot-page request))
    (GET "/register" request (registration-page request)))
