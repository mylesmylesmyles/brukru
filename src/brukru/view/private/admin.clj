(ns brukru.view.private.admin
	"Administrative pages"
	(:require [ring.util.response :as response]
            [compojure.core :refer [defroutes GET POST context]]
            [clojure.tools.logging :refer [debug info error]]
            [hiccup.core :as hiccup]
            [brukru.view.common :refer [restricted admin? authenticated? wrap-layout wrap-context render-template]]
            [brukru.config :refer [PRIVATE-DIR]]
            [brukru.util.time :as time]
            [brukru.util.crypt :as crypt]
            [brukru.model.users :as users]
            [brukru.model.caption :as caption]))


(defn- admin-page-body []
  (hiccup/html [:div
     [:h1 "Admin"]]))

(defn- admin-page [request]
  (wrap-layout "Admin"
               (admin-page-body)))

(defn- list-users-body [users]
    (render-template "admin/list-users.html" {:admin? admin? :users users}))

(defn- list-users-page [params]
	(let [users (caption/get-user-stats)]
		(wrap-layout "Admin - Users"
			(list-users-body users))))

(defn- add-user-page [params]
  (wrap-layout "Admin - Users"
               (render-template "admin/add-user.html" {})))

(defn- save-user [request]
  (wrap-layout "Admin - Users"
       (render-template "admin/add-user.html" {})))

(defn- reset-password [params]
	(when-let [user (users/get-user (Integer/parseInt (params :user-id)))]
		(let [new-password (crypt/random-string 10)]
			(do (users/save-user (assoc user :password (users/encrypt-password user new-password)))
				(wrap-layout "Admin - Reset Password"
	               (render-template "admin/reset-password.html" {:name (user :name) :email (user :email) :new-password new-password}))))))
	
(defroutes admin-routes
	(context "/admin" [] (defroutes admin-routes
		(GET  "/" request (restricted admin? admin-page request))
		(GET  "/users" {params :params} (restricted authenticated? list-users-page params))
		(GET  "/user" {params :params} (restricted admin? add-user-page params))
		(POST "/user" request (restricted admin? save-user request))
		(POST ["/user/:id" :id #"([0-9]+)$"] request (restricted admin? save-user request))
		(POST "/user/resetpassword" {params :params} (restricted admin? reset-password params)))))

