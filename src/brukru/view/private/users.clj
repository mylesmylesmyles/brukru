(ns brukru.view.private.users
    "User profile pages"
  (:require [compojure.core :refer [defroutes GET POST]]
            [ring.util.response :as response]
            [hiccup.core :as hiccup]
            [clojure.pprint :as pprint]
            [clojure.tools.logging :refer [debug info error]]
            [brukru.view.common :refer [restricted admin? authenticated? wrap-layout wrap-context error-alert info-alert]]
            [brukru.view.private.caption :refer [get-picture-href]]
            [brukru.util.session :as session]
            [brukru.util.time :as time]
            [brukru.model.caption :as caption]
            [brukru.model.users :refer [get-user save-user valid-credentials? encrypt-password]]))

(defn- profile-details-form
    [user]
    (hiccup/html
        [:form.form-horizontal {:method "post" :action (wrap-context "/profile")}
            [:legend.muted "Update your profile information"]
            [:div.row-fluid
                [:div.span6
                    [:div.row-fluid.control-group.info
                        [:label.control-label {:for "name"} [:strong "Your name"]]
                        [:div.controls
                            [:span.uneditable-input (:name user)]]]
                    [:div.row-fluid.control-group.info
                        [:label.control-label {:for "nickname"} [:strong "Nickname"]]
                        [:div.controls
                            [:input {:type "text" :name "nickname" :placeholder "Nickname" :value (:nickname user) :required ""}]]]]
                [:div.span6
                    [:div.row-fluid.control-group.error
                        [:label.control-label {:for "old-password"} [:strong "Current password"]]
                        [:div.controls
                            [:input {:type "password" :name "old-password" :placeholder "Old password" :required ""}]]]
                    [:div.row-fluid.control-group.warning
                        [:label.control-label {:for "new-password"} [:strong "New password"]]
                        [:div.controls
                            [:input {:type "password" :name "new-password" :placeholder "New password"}]]]
                    [:div.row-fluid.control-group.warning
                        [:label.control-label {:for "new-password-again"} [:strong "New password again"]]
                        [:div.controls
                            [:input {:type "password" :name "new-password-again" :placeholder "New password again"}]]]]
            [:br]
            [:div.row-fluid.text-center
                [:div.control-group
                    [:input.btn.btn-primary.btn-large {:type "submit" :value "Update" :data-loading-text "Saving..."}]]]]]))

(defn- profile-details
    [user]
    (info (user :lastlogin))
    (hiccup/html
        [:div.row-fluid
            [:div.row-fluid
                [:div.span4.text-right
                    [:strong.text-info "Nickname:"]]
                [:div.span8.text-success (:nickname user)]]
            [:div.row-fluid
                [:div.span4.text-right
                    [:strong.text-info "Last login:"]]
                [:div.span8.text-warning (if (nil? (user :lastlogin))
                    "Never"
                    (time/datestr :pretty  (user :lastlogin)))]]]))

(defn- profile-caption-entry
    [entry counter]
    (hiccup/html
        [(if (= counter 0) :div.item.active :div.item)
            [:img {:src (get-picture-href (entry :picture))}]
            [:div.carousel-caption
                [:h4 (time/datestr :pretty  (entry :createdon))]
                [:p (entry :entry)]]]))

(defn- profile-body [heading user]
    (let [entries (caption/get-entries-for-user user)]
        (hiccup/html
            [:div.row-fluid.span12
                [:div.row-fluid.hero-unit.span12
                    [:h1 heading]
                    (if (= ((session/current-user) :id) (user :id))
                        (profile-details-form user)
                        (profile-details user))]
                [:div.row-fluid 
                    (if (= (count entries) 0)
                        [:h4.text-center "No captions :("]
                        [:div#comments.span12
                            [:h3.text-left (pprint/cl-format nil "~d Caption~:p" (count entries))]
                                [:div#profileCaptions.carousel.slide
                                    [:ol.carousel-indicators
                                        (map #(vector (if (= %1 0) :li.active :li) {:data-target "#profileCaptions" :data-slide-to %1}) (range (count entries)))]
                                    [:div.carousel-inner
                                        (map #(profile-caption-entry %1 %2) entries (range))]
                                    [:a.carousel-control.left {:href "#profileCaptions" :data-slide "prev"} "&lsaquo;"]
                                    [:a.carousel-control.right {:href "#profileCaptions" :data-slide "next"} "&rsaquo;"]]])]])))

(defn- personal-profile-page [request]
    (wrap-layout "Your Profile"
        (hiccup/html 
            [:div
                (when (not (nil? (-> request :flash :error-message)))
                    (error-alert (-> request :flash :error-message)))
                (when (not (nil? (-> request :flash :info-message)))
                    (info-alert (-> request :flash :info-message)))
                (profile-body "Your profile" (session/current-user))])))

(defn- profile-page [id]
    (if-let [user (get-user (Integer/parseInt id))]
        (wrap-layout (str "Profile: " (:name user))
            (profile-body (:name user) user))))

(defn- error-redirect
    ([request message page]
          (assoc (response/redirect (wrap-context page)) :flash {:error-message message :prev-params (request :params)}))
    ([request message]
          (error-redirect request message "/profile")))

(defn- update-profile
    "Update the personal profile of a user"
    [request]
    (if-let [old-password (-> request :params :old-password)]
        (cond
            (not (valid-credentials? ((session/current-user) :password) ((session/current-user) :salt) old-password))
                (error-redirect request "Your old password is invalid")
            (nil? (-> request :params :nickname))
                (error-redirect request "Your can't leave your nickname blank")
            (and (not (nil? (-> request :params :new-password)))
                (not (= (-> request :params :new-password) (-> request :params :new-password-again))))
                (error-redirect request "Your new passwords don't match")
            :else (let [user {:id (:id (session/current-user)) 
                        :nickname (-> request :params :nickname)
                        :password (if (not (clojure.string/blank? (-> request :params :new-password)))
                            (encrypt-password (session/current-user) (-> request :params :new-password))
                            (:password (session/current-user)))}]
                    
                    (save-user user)
                    (if (= ((session/current-user) :id) (user :id))
                        (session/set-user! (get-user (user :id))))
                    (assoc (response/redirect (wrap-context "/profile")) :flash {:info-message "Great success!"})))
        (error-redirect request "You must enter your old password to update your profile")))


(defn- user-line [user]
    (hiccup/html
        [:tr
            [:td 
                [:a {:href (wrap-context (str "/profile/" (user :id)))} (user :id)]]
            [:td.hidden-phone (user :nickname)]
            [:td.hidden-phone (user :name)]
            [:td (if-not (nil? (user :lastlogin))
                        (time/datestr :pretty (user :lastlogin))
                        "Never")]
            [:td 
                [:a {:href (str "mailto:" (user :email))} (user :email)]]
            [:td.hidden-phone (user :total-entries)]
            [:td.hidden-phone (user :avg-rank)]]))

(defn- list-users-body [users]
    (hiccup/html
        [:div.row-fluid
            [:h1 "Users"]
            [:table#list-users.table.table-hover.table-bordered
            [:tr
                [:th "#"]
                [:th.hidden-phone "Nickname"]
                [:th.hidden-phone "Real Name"]
                [:th "Last login"]
                [:th "Email"]
                [:th.hidden-phone "Total Entries"]
                [:th.hidden-phone "Average rank"]]
            (do (map #(user-line %) users))]]))

(defn- list-users-page [params]
    (let [users (caption/get-user-stats)]
        (wrap-layout "Admin - Users"
            (list-users-body users))))
    
(defroutes user-routes
    (POST "/profile" request (restricted authenticated? update-profile request))
    (GET "/profile" request (restricted authenticated? personal-profile-page request))
    (GET ["/profile/:id" :id #"([0-9]+)"] [id] (restricted authenticated? profile-page id))
    (GET  "/users" {params :params} (restricted authenticated? list-users-page params)))

