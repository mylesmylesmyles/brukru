(ns brukru.view.private.caption
  (:require [compojure.core :refer [defroutes GET POST]]
            [ring.util.response :refer [file-response redirect]]
            [hiccup.core :as hiccup]
            [brukru.config :refer [PRIVATE-DIR]]
            [brukru.view.common :refer [wrap-context restricted authenticated? admin? wrap-layout]]
            [brukru.model.caption :as caption]
            [brukru.util.session :as session]
            [brukru.util.image :as image]
            [clojure.tools.logging :refer [debug info error]]
            [clojure.java.io :refer [copy delete-file]])
    (:import [java.io File]))

(defn get-picture-thumbnail-href
  "Return the thumbnail href of a picture for picture ID"
  ([^Integer picture-id ^Integer size] (wrap-context (str "/caption/img/thumb/" picture-id ".jpg?width=" size)))
  ([^Integer picture-id] (get-picture-thumbnail-href picture-id image/default-thumb-size)))

(defn get-picture-href
  "Return the href of a picture for picture ID"
  ([^Integer picture-id] (wrap-context (str "/caption/img/" picture-id ".jpg"))))

(defn caption-form-body [picture]
  (hiccup/html
    [:div.caption-form2.text-center
      [:form.form-inline {:method "post" :action (wrap-context "/caption/")}
        [:input {:type "hidden" :name "id" :value (picture :id)}]
        [:div.row-fluid
          [:div.span6
            [:textarea.span12 {:rows 2 :cols 20 :name "caption" :placeholder "What's it look like to you?" :required ""}]]
          [:div.span6
            [:button.btn.btn-info {:type "submit"} "Add Caption"]]
        ]
    ]]))

(defn- render-caption-image 
  ([id]
    (when-let [picture (caption/get-picture (Integer/parseInt id))]
      (file-response (picture :location) {:root PRIVATE-DIR})))
  ([]
    (render-caption-image (:id (caption/get-current-picture)))))

(defn str->int [str] (when (and (not (nil? str)) (re-matches (re-pattern "\\d+") str)) (read-string str)))

(defn- render-caption-image-thumb 
  ([id width]
    (let [width (if (or (nil? width) (nil? (str->int width))) image/default-thumb-size (Integer. (str->int width)))]
        (if-let [picture (caption/get-picture (Integer/parseInt id))]
          {:status 200
            :headers {"Content-Type" "image/jpeg"}
            :body (image/get-thumbnail-inputstream (str PRIVATE-DIR "/" (picture :location)) width)})))
  ([id] (render-caption-image-thumb id 100)))

(defn- add-caption [params]
  (let [ user (session/current-user)
         entry {:picture (Integer/parseInt (params :id)) :entry (params :caption) :author (user :id)}]
    (info entry)
    (when (not (clojure.string/blank? (entry :entry)))
      (caption/save-entry entry))
    (redirect (wrap-context "/"))))

(defn- vote-for-caption [id]
  "Vote for a caption"
  (if-let [entry (caption/get-entry (Integer/parseInt id))]
    (caption/vote-for-entry entry (session/current-user))
    (redirect (wrap-context "/")))
  (redirect (wrap-context "/")))

(defn- list-caption-entries-body []
  (hiccup/html
    [:div
     [:h1 "Caption entries"]]))

(defn- list-caption-entries-page [params]
  (wrap-layout "Caption entries"
               (list-caption-entries-body)))

(defn- add-caption-entry-body []
  (hiccup/html
    [:div.row-fluid
     [:h1 "Admin - Add caption-entry"]
     [:form {:action (wrap-context "/admin/upload") :method "POST" :enctype "multipart/form-data" }
      [:div.fileupload.fileupload-new {:data-provides "fileupload"}
        [:div.fileupload-preview.thumbnail {:style "width: 200px; height: 150px"}]
        [:div
          [:span.btn.btn-file
            [:span.fileupload-new "Select image"]
            [:span.fileupload-exists "Change"]
            [:input {:name "file" :type "file" :size "20"}]]
          [:a.btn.fileupload-exists {:href "#" :data-dismiss "fileupload"} "Remove"]
          [:input.btn.fileupload-exists {:type "submit" :value "Upload"} ]]]]]))

(defn- add-caption-entry-page [params]
  (wrap-layout "Caption entries"
               (add-caption-entry-body)))

(defn- save-caption-entry [params]
  (info params)
  (when-let [file (-> params :file)]
    (do 
      (copy (file :tempfile) 
            (File. (str PRIVATE-DIR "/caption/img/" (file :filename))))
      (delete-file (file :tempfile))
      (caption/save-entry {:location (str "/caption/img/" (file :filename)) :canaddcaptions 1})
      (redirect (wrap-context "/caption/entries"))))
)

(defroutes caption-routes
  (POST "/caption/" {params :params} (restricted authenticated? add-caption params))
  (POST ["/caption/vote/:id" :id #"([0-9]+)$"] [id] (restricted authenticated? vote-for-caption id))
  (GET  "/caption/entries" {params :params} (restricted admin? list-caption-entries-page params))
  (GET  "/caption/entry" {params :params} (restricted admin? add-caption-entry-page params))
  (POST "/caption/entry" {params :params} (restricted admin? save-caption-entry params))
  (GET ["/caption/img/thumb/:id" :id #"([0-9]+)\.jpg$"] [id width] (restricted authenticated? render-caption-image-thumb  (first(clojure.string/split id #"\.")) width))
  (GET ["/caption/img/:id" :id #"([0-9]+)\.jpg$"] [id] (restricted authenticated? render-caption-image (first(clojure.string/split id #"\.")))))