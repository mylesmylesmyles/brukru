(ns brukru.view.home
  (:require [compojure.core :refer [defroutes GET]]
            [clojure.tools.logging :refer [debug info error]]
            [hiccup.core :as hiccup]
            [brukru.view.common :refer [wrap-context restricted authenticated? wrap-layout]]
            [brukru.model.caption :refer [get-current-picture get-entries]]
            [brukru.view.private.caption :refer [get-picture-href caption-form-body]]
            [brukru.util.time :as time]
            [clojure.pprint :as pprint]))

(defn- get-picture-location [picture]
  (wrap-context (str "/caption/img/" (picture :id) ".jpg")))

(defn- entry-body [entry & {:keys [even-odd]}]
  (hiccup/html
    [:form.form-inline {:action (wrap-context (str "/caption/vote/" (entry :id))) :method "post" :style "margin:0"}
      [:blockquote
        [:p 
          [:button.btn.btn-success {:type "submit" :title "I like it!"} [:i.icon-arrow-up]]
          " " (entry :entry)]
        [:small 
          [:a {:href (wrap-context (str "/profile/" (entry :user-id)))}
            [:abbr {:title (entry :user-name)} 
                (entry :user-nickname)]]
          " on " 
          (time/datestr :pretty (entry :createdon)) ]]]))


(defn- page-body []
  (let [picture (get-current-picture)
      location (get-picture-href (picture :id))
      entries (get-entries picture)]
    (hiccup/html 
      [:div.row-fluid
          ; [:div.span12.text-center [:h1 "Home"]]
          [:br]
          [:div.row-fluid
            [:div.span7.hero-unit
              [:div.row-fluid
                [:a {:href location} [:img.img-polaroid {:src location}]]]
              [:div.row-fluid [:br]]
              [:div.row-fluid
                (caption-form-body picture)]]
            [:div#comments.span5
              [:h3.text-center (pprint/cl-format nil "~d Caption~:p" (count entries))]
              (map #(entry-body %1 :even-odd %2) entries (cycle ["even" "odd"]))]]])))

(defn- render-page [request]
  (wrap-layout "brukru" (page-body)))

(defroutes home-routes
  (GET "/" request (restricted authenticated? render-page request)))
