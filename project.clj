(defproject brukru "0.8.0-SNAPSHOT"
  :description "brukru - the webapp"
  :url "http://www.brukru.com"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :profiles {:dev { :dependencies [[org.clojure/clojure "1.5.1"]
                                  [clj-time "0.6.0"]
                                  [ns-tracker "0.2.1"]
                                  [ring "1.2.1" :exclusions [ns-tracker]]
                                  [compojure "1.1.6"]
                                  [clabango "0.5"]
                                  [hiccup "1.0.4"]
                                  ; [com.h2database/h2 "1.3.170"]
                                  [mysql/mysql-connector-java "5.1.27"]
                                  [postgresql "9.1-901-1.jdbc4"]
                                  [korma "0.3.0-RC5"]
                                  [org.clojure/tools.logging "0.2.6"]
                                  [http-kit "2.1.13"]]
                    :plugins [[lein-ring "0.8.7"]
                              [lein-pprint "1.1.2-SNAPSHOT"]
                              [lein-ancient "0.5.4"]]}
              :setup [:dev {:dependencies [
                                          ; [clj-yaml "0.3.1"]
                                          [slamhound "1.3.1"]
                                          [lobos "1.0.0-beta1"]]
                           :plugins [[lein-localrepo "0.4.1"]]}]}
  :aliases {"launch" ["run" "-m" "brukru.server"]
            "dumbrepl" ["trampoline" "run" "-m" "brukru.server/main"]
            "setup" ["with-profile" "setup" "repl"]
            "slamhound" ["run" "-m" "slam.hound"]
            "publish" ["with-profile" "dev" "uberjar"]
            "test!" ["do" "clean," "deps," "test"]}
  :ring {:handler brukru.app/site-handler}
  :aot [brukru.server]
  :main brukru.server
  :test-selectors { :default (complement :integration)
                    :integration :integration
                    :all (constantly true)}
  :min-lein-version "2.0.0"
  :repl-options { :prompt (fn [ns] (str "<" ns "> You're awesome ;) " ))
                  :port 4001
                  :host "127.0.0.1"}
  :local-repo "C:\\Users\\Myles\\.m2\\repository"
  ; :global-vars {*warn-on-reflection* true}
  ; :local-repo "/home/brukru/.m2/repository"
  ; :update :always
  ; :jar-name "brukru.jar"
  ; :bootclasspath true
  ; Set arbitrary key/value pairs for the jar's manifest.
  ; :manifest {;; Function values will be called with the project as an argument.
  ;            "Class-Path" ~#(clojure.string/join
  ;                            \space
  ;                            (leiningen.core.classpath/get-classpath %))}
)