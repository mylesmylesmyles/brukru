## brukru

A simple little picture caption app.  This was my first attempt at learning Clojure.  I'm sure there are a lot of mistakes and it is very outdated. 

## Installation

Download and install the following:

* Java - http://www.oracle.com/technetwork/java/index.html
* Leingingen - http://leiningen.org/
* Database - H2 http://www.h2database.com/html/main.html or Mysql 

Next you will have to install the dependencies by running the following on the command line inside the base directory of the app:

> lein deps

## Database configuration

You will need to update the src/brukru/config.clj and set your database configuration.

# Mysql
(def DEVDBCONFIG {
	:classname "com.mysql.jdbc.Driver"
    :subprotocol "mysql"
	:subname "//127.0.0.1:3306/[db name]"
	:user "[username]"
	:password "[password]"
})

# H2

> lein localrepo install .\resources\private\h2-1.3.170.jar com.h2database/h2 1.3.170

(def DEVDBCONFIG {
    :classname   "org.h2.Driver"
    :subprotocol "h2"
    :subname     (str PRIVATE-DIR "/" "brukru_db")
	:user "[username]"
	:password "[password]"
})

## Database setup

# Create the initial tables
> lein with-profile setup repl
(use 'lobos.core 'lobos.connectivity 'lobos.migration 'lobos.migrations 'brukru.config)
(open-global DEVDBCONFIG)
(migrate)

# Create a new user
> lein with-profile setup repl

; Use the crypt library for password encryption
(require '[brukru.util.crypt :as crypt])

; Use the users library to create a user
(require '[brukru.model.users :as users])

; Salt for the password
(def salt (crypt/gen-salt 70))

; Set the password
(def pw (crypt/encrypt salt "mypassword"))

; Create the new user definition
(def me {:id nil :email "youremail@domain.com" :password (crypt/byte-to-base64 pw) :salt (crypt/byte-to-base64 salt) :name "Your Full Name" :nickname "Me!" :validemail true :disabled false})

; Save the user
(users/save-user me)

# Create the first picture entry
> lein with-profile setup repl
(require '[korma.core :refer :all])
(use 'brukru.model.caption)
(def pic {:id nil :location "/img/caption/first.jpg"})
(insert picture (values pic))

## Running

Now you're ready to view the website on the default port 8080

> lein run

To change the port

> lein run 12345

## License

Copyright � 2012 Myles Goodhue <myles.goodhue@gmail.com>

Distributed under the Eclipse Public License, the same as Clojure.