(ns brukru.config
  (:use (korma [db :only (defdb mysql postgres)])))

(def production?
  (= "production" (get (System/getenv) "APP_ENV")))

(def development?
  (not production?))

(def DEBUG development?)

(def TIME-ZONE "Canada/Eastern")
(def TIME-FORMAT "MMMM dd, yyyy @ h:mm a z")

(def LOCALE (java.util.Locale/ENGLISH))

; Resource directories
(def PUBLIC-DIR (str (System/getProperty "user.dir") "/resources/public"))
(def PRIVATE-DIR (str (System/getProperty "user.dir") "/resources/private"))

; Database connections
(def DEVDBCONFIG {:classname "com.mysql.jdbc.Driver"
                :subprotocol "mysql"
				:subname "//127.0.0.1:3306/piction"
				:user "piction"
				:password "ca2bJdHu"})

(def PRODDBCONFIG {
    :classname "org.postgresql.Driver"
    :subprotocol "postgresql"
    :subname "//localhost:5432/brukru"
    :db "brukru"
    :user "brukru"
    :password (get (System/getenv) "DB_PASSWORD")
    :host "localhost"
    :port "5432"})

(defdb DB (if development?
                (mysql DEVDBCONFIG)
                (postgres PRODDBCONFIG)))

; (def DEVDBCONFIG {
;     :classname   "org.h2.Driver"
;     :subprotocol "h2"
;     :subname     (str PRIVATE-DIR "/" "brukru_db")
; 	:user "[username]"
; 	:password "[password]"})
; 
; (defdb DB (if development?
; 				DEVDBCONFIG
; 				(mysql PRODDBCONFIG)))   