(ns brukru.app
  (:require [compojure.core :refer [defroutes routes context]]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [brukru.middleware.core :refer [wrap-if wrap-failsafe wrap-bounce-favicon]]
            [brukru.middleware.context :refer [wrap-context]]
            [brukru.middleware.log :refer [wrap-request-logging wrap-exception-logging]]
            [brukru.config :refer [development? production?]]
            [ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.stacktrace :refer [wrap-stacktrace]]))


;; Initialization
; Add required code here (database, etc.)


;; Load public routes
(require '[brukru.view.home :refer [home-routes]]
         '[brukru.view.about :refer [about-routes]])

;; Load authentication routes
(require '[brukru.view.auth :refer [auth-routes]])

;; Load private routes
(require '[brukru.view.private.users :refer [user-routes]]
         '[brukru.view.private.admin :refer [admin-routes]]
         '[brukru.view.private.caption :refer [caption-routes]])


;; Ring handler definition
(defroutes site-handler
  (-> (routes home-routes
              about-routes
              caption-routes
              auth-routes
              user-routes
              admin-routes
              (route/resources "/")
              (route/not-found "<h1>Page not found so quit trying to look for it.</h1>"))
      (wrap-if development? wrap-reload '[brukru.middleware brukru.app])
      (wrap-if development? wrap-request-logging)
      (wrap-if development? wrap-stacktrace)
      (wrap-if production? wrap-failsafe)
      (wrap-if development? wrap-exception-logging)
      (wrap-bounce-favicon)
      (wrap-context)
      (handler/site)))
