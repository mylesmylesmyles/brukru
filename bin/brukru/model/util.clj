(ns brukru.model.util
	(:require [korma.core :refer :all]))

(defn update? 
	"Conditionally apply the function to the field if the field is present:
    map key function"
    [m k func]
	(if (and m k (m k))
		(update-in m [k] func)
		m))

(defmulti clob-to-string (fn [x] (class x)))
(defmethod clob-to-string java.sql.Clob
	[clob]
	(with-open [rdr (java.io.BufferedReader. (.getCharacterStream clob))]
		(apply str (line-seq rdr))))
(defmethod clob-to-string java.lang.String
	[clob] clob )