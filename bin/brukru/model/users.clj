(ns brukru.model.users
	(:require [brukru.config :as config]
        [clojure.tools.logging :refer [debug info error]]
		[korma.core :refer :all]
		[korma.db :refer [transaction]]
		[brukru.util.crypt :as crypt]
		[clj-time.coerce :refer [to-timestamp]]
		[clj-time.local :refer [local-now]]
		[clj-time.core :refer [minus minutes]]))

(defentity users)

(defentity active-users
  (table (subselect users
            (where {:disabled false})
            (order :nickname :ASC))
         :active-users))

; A vote for a caption entry
(defentity user-logins
	(table :user_logins)
	(belongs-to users {:fk :users_id}))

; Maximum allowed login attempts before temporarily locking account
(def max-login-attempts 5)

; Number of minutes to lock the account
(def login-lockout-duration 5)

(defn get-user 
	"Get user by id"
	[id]
	(transaction (first (select active-users (where {:id id}) ))))

(defn get-users
	"Get a list of users"
	[]
	(transaction (select users (order :nickname :ASC))))

(defn get-user-by-email 
	"Get a user by their email address"
	[email]
	(transaction (first (select active-users (where {:email email})))))

(defn user-exists?
	"Does a user exist?"
	[usr]
	(or (nil? (usr :id)) (not (empty? (get-user (usr :id))))))

(defn save-user 
	"Save a user (update or insert)"
	[usr]
	(transaction
		(if (or (user-exists? usr) (not (empty? (get-user-by-email (usr :email)))))
			(update users (set-fields usr) (where {:id (usr :id)}))
			(insert users (values usr)))))

(defn too-many-login-attempts? 
	"Has the user made too many unsuccessful login attempts?"
	[id]
	(let [ delta (to-timestamp (minus (local-now) (minutes login-lockout-duration)))
		   result (transaction
		   			(first 
						(select user-logins (aggregate (count :*) :cnt) 
								(where {:users_id id
										:ondate [> delta]}))))
		   total (if (nil? result) 0 (result :cnt))]
		(> total (- max-login-attempts 1))))

(defn record-login-failure
	[id]
	(transaction (insert user-logins (values {:users_id id}))))

(defn valid-credentials? 
	"Validate the user credentials for login"
	[^String password ^String salt ^String test-password]
	(crypt/compare test-password (crypt/base64-to-byte password) (crypt/base64-to-byte salt)))

(defn encrypt-password 
	"Encrypt a user's password"
	[usr password]
	(crypt/byte-to-base64 (crypt/encrypt (crypt/base64-to-byte (:salt usr)) password)))

(defn create-salt [email]
	"Create a salt for a user"
	(let [salt (crypt/gen-salt 32)]
		(crypt/byte-to-base64 (crypt/encrypt salt email))))
	