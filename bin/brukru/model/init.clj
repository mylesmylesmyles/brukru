(ns brukru.model.init
	(:require [clj-yaml.core :refer :all]
		[brukru.util.crypt :as crypt]))

(defn createdb []
	(let [salt (crypt/gen-salt)
		users (parse-string (slurp "./resources/fixtures.yml"))]
		(insert users (values (:users users)))))
