(ns brukru.view.common
    (:require [ring.util.response :as response]
            [hiccup.core :as hiccup]
            [hiccup.page :as page]
            [hiccup.element :as element]
            [hiccup.util :as util]
            [clabango.parser :as parser]
            [brukru.util.session :as session]
            [brukru.config :refer [PRIVATE-DIR]]
            [clojure.tools.logging :refer [debug info error]]))

;;; Context utils
(defn wrap-context
  "Add web context to the path of URI"
  [path]
  (-> path
      (util/to-uri)
      (util/to-str)))


;;; HTML utils
(defmacro defhtml [name params & content]
  `(defn ~name ~params
     (hiccup/html ~@content)))

;;; User utils
(defn restricted
  "Function for restricted part of the Web site. 
   Takes a predicate function and the handler to execute if predicate is true."
  [predicate handler & args]
  (if (predicate)
     (apply handler args)
     (response/redirect (wrap-context "/login"))))

(defn authenticated?
  "Sample authentication function. Test if current user is not null."
  []
  (not (nil? (session/current-user))))

(defn admin?
  "Sample authorization function. Test if current user it admin."
  []
  (if-let [user (session/current-user)]
    (= 1 (user :id))))


;;; Page utils

(defn render-template [template params]
    (parser/render (slurp (str PRIVATE-DIR java.io.File/separator "templates" java.io.File/separator template)) params))

(defn- nav-bar []
    (render-template "nav.html" { :admin? (admin?) :nickname (:nickname (session/current-user)) }))

(defn wrap-layout
    "Define pages layout"
    [title & body]
    (info (first body))
    (render-template "layout.html" {:title title :content (first body) :nav-bar (when (authenticated?) (nav-bar))}))

(defhtml error-alert [message & {:keys [heading] :or {heading "Oh snap!  You got an error!"}}]
  (render-template "error-alert.html" {:heading heading :message message}))

(defhtml info-alert [message & {:keys [heading] :or {heading "Woohoo!"}}] 
  (render-template "info-alert.html" {:heading heading :message message}))

