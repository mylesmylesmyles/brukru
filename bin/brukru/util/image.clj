(ns brukru.util.image
	(:require [brukru.config :as config]
            	[clojure.tools.logging :refer [debug info error]])
	(:import (javax.imageio ImageIO)
			(java.io ByteArrayInputStream ByteArrayOutputStream)
			(java.awt.image BufferedImage)
			(java.awt Graphics2D)
			(java.io File)))

; Maximum thumbnail size
(def max-thumb-size 800)

; Default thumbnail size
(def default-thumb-size 100)

(defn- to-buffered-image 
	"Convert an ToolKitImage to a BufferedImage"
	[img]
	(let [	bimage (BufferedImage. (.getWidth img nil) (.getHeight img nil) BufferedImage/TYPE_INT_BGR)
			bgr    (.createGraphics bimage)]
		(do (.drawImage bgr img 0 0 nil)
			(.dispose bgr)
			bimage)))

(defn get-thumbnail-inputstream 
	"Convert an image to a thumbnail and return it as an input stream"
	([location ^Integer width]
		(let [	width (if (> width max-thumb-size) max-thumb-size width)
				bi    (ImageIO/read (File. location))
				ratio (/ width (.getWidth bi))
				image (to-buffered-image (.getScaledInstance bi (* (.getWidth bi) ratio) (* (.getHeight bi) ratio) BufferedImage/SCALE_SMOOTH))
				os    (ByteArrayOutputStream.)]
			(when (ImageIO/write image "jpeg" os)
				(ByteArrayInputStream. (.toByteArray os)))))
	([location] (get-thumbnail-inputstream location default-thumb-size)))