(ns brukru.util.session
  (:require [brukru.middleware.context :as context]))

(defn set-user! [user]
  (context/session-put! :user user))

(defn current-user
  "Retrieve current user"
  []
  (context/session-get :user))

(defn logout
  "Reset session"
  []
  (context/session-clear))