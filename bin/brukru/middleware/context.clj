(ns brukru.middleware.context
  (:require [clj-time.core :refer [before? minus minutes]]
    [clj-time.local :refer [local-now]]))

(declare ^:dynamic *session*)
(declare session-expired?)
(declare session-clear)
(declare touch-session!)

(def timeout 30)

(defn- now [] (local-now))

(defn wrap-context
  "Store request, session and flash into a Clojure map"
  [handler]
  (fn [request]
    (binding [*session* (atom {})]
      (when-let [session (get-in request [:session :app-context])]
        (reset! *session* session))
      (when (session-expired?) 
            (session-clear))
      (touch-session!)
      (let [response (handler request)
            session (:session response)]
        (assoc-in response [:session :app-context] @*session*)))))

(defn session-put!
  "Add or update key/value for the current session"
  [k v]
  (swap! *session* (fn [old-session]
                     (assoc old-session k v))))

(defn session-get
  "Get the value associated to a key for the current session"
  [k]
  (@*session* k))

(defn- touch-session! [] (session-put! :created (now)))

(defn session-clear
  "Clear the current session"
  []
  (reset! *session* {}))

(defn session-expired?
  "Has the current session expired"
  []
  (or (nil? (session-get :created)) (before? (session-get :created) (minus (now) (minutes timeout)))))