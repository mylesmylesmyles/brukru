(comment "This file defines the migrations for the brukru database.
          To run the migrations open the Clojure REPL and run the following code:
          (use 'lobos.core 'lobos.connectivity 'lobos.migration 'lobos.migrations)
          (open-global dbconfig)
          (migrate)
          ")
(ns lobos.migrations
  ;; exclude some clojure built-in symbols so we can use the lobos' symbols
  (:refer-clojure :exclude [alter drop
                            bigint boolean char double float time])
  (:require [clojure.core :as clj]) ;; allow to still reach clojure.core/time through clj/time
  ;; use only defmigration macro from lobos
  (:use (lobos [migration :only [defmigration]]
          core
          schema)
        (brukru [config :as config])))

;; database configuration
(def dbconfig config/DEVDBCONFIG)

(defmigration add-users-table
  ;; code be executed when migrating the schema "up" using "migrate"
  (up [] 
      (create dbconfig
              (table :users (integer :id :primary-key :auto-inc)
                      (varchar :email 320 )
							        (varchar :password 100 :not-null)
							        (timestamp :lastlogin)
							        (varchar :name 100 :not-null)
							        (varchar :nickname 100)
							        (boolean :validemail (default false))
							        (boolean :disabled (default false))
                      (varchar :salt 100 :not-null))))
  ;; Code to be executed when migrating schema "down" using "rollback"
  (down [] 
        (drop 
          (table :users )))) 

(defmigration add-caption-picture
  (up [] (create dbconfig
                 (table :caption_picture (integer :id :primary-key :auto-inc)
                        (timestamp :createdon (default (now)))
                        (varchar :location 200 :not-null)
                        (boolean :canaddcaptions (default true)))))
  (down [] (drop (table :caption_picture))))

(defmigration add-caption-entry
  (up [] (create dbconfig
                 (table :caption_entry (integer :id :primary-key :auto-inc)
                        (timestamp :createdon (default (now)))
                        (text :entry :not-null)
                        (double :rank (default 0))
                        (integer :author [:refer :users :id] :not-null)
                        (integer :picture [:refer :caption_picture :id] :not-null))))
  (down [] (drop (table :caption_entry))))


(defmigration add-caption-entry-vote
  (up [] (create dbconfig
                 (table :caption_entry_vote (integer :id :primary-key :auto-inc)
                        (timestamp :createdon (default (now)))
                        (integer :author [:refer :users :id] :not-null)
                        (integer :entry [:refer :caption_entry :id] :not-null))))
  (down [] (drop (table :caption_entry_vote))))